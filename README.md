# 自媒体站点模板

#### 介绍
使用layui自适应框架、swiper轮播插件构建的响应式文章管理模板。

#### 展示
<img src="https://images.gitee.com/uploads/images/2020/0327/162343_956e64dd_2251197.jpeg">

<img src="https://images.gitee.com/uploads/images/2020/0327/162343_8c131196_2251197.jpeg">

<img src="https://images.gitee.com/uploads/images/2020/0327/162526_7dd83d4e_2251197.jpeg">


